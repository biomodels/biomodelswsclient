/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package uk.ac.ebi.biomodels.ws.dao.config

import spock.lang.Specification

/**
 * @author Mihai Glonț mglont@ebi.ac.uk
 * @author Tung Nguyen tnguyen@ebi.ac.uk
 */
class ConfigSpec extends Specification {
    def "test Config class"() {
        when: "we instantiate a devConfig and a prodConfig"
        Config devConfig = new DevConfig()
        Config prodConfig = new ProdConfig()

        then: "the objects are constructed properly"
        devConfig.protocol == prodConfig.protocol
        devConfig.portNumber == prodConfig.portNumber
        devConfig.hostName.contains("wwwdev")
        prodConfig.hostName.contains("www.ebi.ac.uk")
    }

    def "test BioModels Endpoint Prefix"() {
        given: "a specific Config object"
        Config devCfg = new DevConfig()

        expect: "the prefix of the web service end point should contain 'biomodels'"
        devCfg.endPointPrefix.contains("biomodels")
    }
}
