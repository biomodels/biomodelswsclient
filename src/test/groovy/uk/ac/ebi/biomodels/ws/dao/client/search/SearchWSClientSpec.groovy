/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package uk.ac.ebi.biomodels.ws.dao.client.search

import spock.lang.Specification
import uk.ac.ebi.biomodels.ws.dao.config.Config
import uk.ac.ebi.biomodels.ws.dao.config.ProdConfig

/**
 * @author Mihai Glonț mglont@ebi.ac.uk
 * @author Tung Nguyen tnguyen@ebi.ac.uk
 */
class SearchWSClientSpec  extends Specification {
    Config config = new ProdConfig()
    SearchWsClient searchWsClient

    def setup() {
        searchWsClient = new SearchWsClient(config)
    }

    def "test how to search models from BioModels with search terms"() {
        when: "feed a search term"
        String searchTerm = "MAPK"
        and: "make a request to BioModels Web Service"
        SearchResult result = searchWsClient.search(searchTerm)

        then: "check the result"
        result.matches != null
    }

    def "test how to fetch the identifiers of all curated models"() {
        when: "send a request of fetching all curated models"
        String[] result = searchWsClient.getAllCuratedModelsId()
        for (it in result) {
            println it
        }

        then: "check the result"
        // the default number of results if it is not there in the URL
        println result.length
        result.length > 700
    }
}
