/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package uk.ac.ebi.biomodels.ws.dao.client.model

import spock.lang.Specification
import uk.ac.ebi.biomodels.ws.dao.config.Config
import uk.ac.ebi.biomodels.ws.dao.config.ProdConfig
import uk.ac.ebi.biomodels.ws.dao.model.ModelFiles
import uk.ac.ebi.biomodels.ws.SimpleModel

/**
 * @author Mihai Glonț mglont@ebi.ac.uk
 * @author Tung Nguyen tnguyen@ebi.ac.uk
 */
class ModelWsClientSpec extends Specification {
    Config config = new ProdConfig()
    ModelWsClient modelWsClient

    def setup() {
        modelWsClient = new ModelWsClient(config)
    }

    def "test how to get a given model from BioModels with identifier"() {
        when: "give a model identifier"
        String modelId = "BIOMD0000000640"
        and: "make a request to BioModels Web Service"
        SimpleModel model = modelWsClient.getModel(modelId)
        then: "return a result"
        model != null
        model.publication.get("link").asText() == "http://identifiers.org/pubmed/27869123"
        model.publicationId == "27869123"
        model.id == modelId
        model.submissionId == "MODEL1702270000"
        model.files.main.size() == 1
        model.files.additional.size() == 9
        model.history.revisions.size() == 2
    }

    def "test how to get the metadata of the files associated with the model"() {
        when: "give a model identifier"
        String modelId = "BIOMD0000000640"
        and: "make a request to BioModels Web Service"
        ModelFiles files = modelWsClient.getModelFiles(modelId)
        then: "return a result"
        files != null
        files.main.size() == 1
        files.additional.size() == 9
        files.main[0].name == modelId.concat("_url.xml")
    }
    
    def "test how to return Model SBML as a string"() {
        when: "give a model identifier"
        String modelId = "BIOMD0000000640"
        and: "make a request to BioModels Web Service"
        String result = modelWsClient.getModelSBMLById(modelId)
        then: "return a result"
        result != null
    }

    def "test how to fetch publication with the model"() {
        when: "given a model identifier"
        String modelId = "BIOMD0000000470" // its publication is an DOI
        and: "send a request to BioModels Web Service to fetch the model"
        SimpleModel model = modelWsClient.getModel(modelId)
        then: "expect the following tests passed"
        String publicationId = model.publicationId
        publicationId == "10.1089/ind.2013.0003"
    }
}
