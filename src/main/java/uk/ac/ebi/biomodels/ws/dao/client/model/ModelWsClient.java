/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package uk.ac.ebi.biomodels.ws.dao.client.model;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.http.*;
import uk.ac.ebi.biomodels.ws.BioModelsWSException;
import uk.ac.ebi.biomodels.ws.dao.config.Config;
import uk.ac.ebi.biomodels.ws.SimpleModel;
import uk.ac.ebi.biomodels.ws.dao.model.ModelFile;
import uk.ac.ebi.biomodels.ws.dao.model.ModelFiles;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.web.client.RestClientException;
import uk.ac.ebi.biomodels.ws.dao.client.BmWsClient;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author Mihai Glonț mglont@ebi.ac.uk
 * @author Tung Nguyen tnguyen@ebi.ac.uk
 */
public class ModelWsClient extends BmWsClient {
    private static final Logger logger = LogManager.getLogger(ModelWsClient.class.getName());

    public ModelWsClient(Config config) {
        super(config);
    }

    public SimpleModel getModel(String modelId) throws RestClientException {
        final String url = String.format("%s/%s?format=json", config.getEndPointPrefix(), modelId);
        SimpleModel result = this.restTemplate.getForObject(url, SimpleModel.class);
        result.setId(result.getPublicationId());
        /**
         * Web Services of the new platform returns an identifiers-based publication (i.e. an url) while the old one
         * copes with the only accession of the full url publication.
         */
        JsonNode pubJsonNode = result.getPublication();
        String pubLink = pubJsonNode.get("link").asText();
        if (pubLink.length() > 0) {
            String collectionAndAccession = pubLink.split("://identifiers.org/")[1];
            String collection = collectionAndAccession.substring(0, collectionAndAccession.indexOf("/"));
            String accession = collectionAndAccession.substring(collectionAndAccession.indexOf("/") + 1);
            String publicationId = accession;
            result.setPublicationId(publicationId);
        }
        logger.debug("The model {} which has just been fetched has the name '{}'", modelId, result.getName());
        return result;
    }

    public String getModelNameByModelId(String modelId) {
        SimpleModel simpleModel = getModel(modelId);
        return simpleModel.getName();
    }

    public ModelFiles getModelFiles(String modelId) {
        final String url = String.format("%s/model/files/%s?format=json", config.getEndPointPrefix(), modelId);
        ModelFiles result = this.restTemplate.getForObject(url, ModelFiles.class);
        int nbMain = result.getMain().size();
        int nbAdditional = result.getAdditional().size();
        logger.debug("The model {} which has just been fetched has {} main file(s) and {} additional file(s)", modelId, nbMain, nbAdditional);
        return result;
    }

    public String getModelSBMLById(String modelId) throws BioModelsWSException, IOException {
        String result = "";
        String mainFileName = getMainFileName(modelId);
        String url = String.format("%s/model/download/%s?filename=%s",
                config.getEndPointPrefix(), modelId, mainFileName);
        /**
         * For example:
         * url = "https://www.ebi.ac.uk/biomodels/model/download/BIOMD0000000705.3?filename=BIOMD0000000705_url.xml";
         * We don't check the revision number because the latest revision is always served
         */
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<String> response = this.restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        if (response.getStatusCode() == HttpStatus.OK) {
            // if we need to output the response to a file. uncomment the following statement
            // Files.write(Paths.get(mainFileName), response.getBody());
            result = response.getBody();
        }
        logger.debug("File content:\n{}", result);
        return result;
    }

    public String getPublicationId(String modelId) throws BioModelsWSException, IOException {
        SimpleModel simpleModel = getModel(modelId);
        return simpleModel.getPublicationId();
    }

    private String getMainFileName(String modelId) {
        ModelFiles files = getModelFiles(modelId);
        ModelFile mainFile = files.getMain().get(0);
        return mainFile.getName();
    }
}
