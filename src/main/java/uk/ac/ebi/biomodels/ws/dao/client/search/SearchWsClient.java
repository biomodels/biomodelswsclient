/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package uk.ac.ebi.biomodels.ws.dao.client.search;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.client.RestClientException;
import uk.ac.ebi.biomodels.ws.dao.client.BmWsClient;
import uk.ac.ebi.biomodels.ws.dao.config.Config;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Mihai Glonț mglont@ebi.ac.uk
 * @author Tung Nguyen tnguyen@ebi.ac.uk
 */
public class SearchWsClient extends BmWsClient {
    private static final Logger logger = LogManager.getLogger(SearchWsClient.class.getName());

    public SearchWsClient(Config config) {
        super(config);
    }

    private String searchURLPrefix() {
        return String.format("%s/search?query", config.getEndPointPrefix());
    }
    public SearchResult search(String searchTerm) throws RestClientException {
        final String url = String.format("%s=%s?format=json", searchURLPrefix(), searchTerm);
        SearchResult result = this.restTemplate.getForObject(url, SearchResult.class);
        return result;
    }

    public String[] getAllCuratedModelsId() {
        final int MAXSIZE = 100;
        int offset = 0;
        int numResult = MAXSIZE;
        String url = String.format("%s=%s&offset=%d&numResults=%d&format=json", searchURLPrefix(), "curationstatus:Manually curated", offset, numResult);
        SearchResult searchResult = this.restTemplate.getForObject(url, SearchResult.class);
        /*String searchTerm = "curationstatus:Manually curated";
        SearchResult searchResult = search(searchTerm);*/
        int matchCount = searchResult.matches;
        String[] result = new String[matchCount];
        logger.debug("The number of models match the query {}", matchCount);
        int summaryIndex = -1;
        for (ModelSummary model: searchResult.models) {
            logger.debug(model.id, " ", model.name);
            result[++summaryIndex] = model.id;
        }
        if (matchCount > 0) {
            int matches = matchCount;
            if (matches < MAXSIZE) {
                result = extractModelId(searchResult.models);
            } else {
                // make multiple requests to the server
                int nLoop = (int) Math.ceil(matches / (MAXSIZE*1.0));
                // we already have the first results page, now load the rest
                for (int i = 1; i < nLoop; i++) {
                    offset += MAXSIZE;
                    url = String.format("%s=%s&offset=%d&numResults=%d&format=json", searchURLPrefix(), "curationstatus:Manually curated", offset, numResult);
                    searchResult = this.restTemplate.getForObject(url, SearchResult.class);
                    String[] modelIDs = extractModelId(searchResult.models);
                    logger.info("modelIDs {}\tresult {}\toffset{}\tsummaryIndex {}",
                            modelIDs.length, result.length, offset, summaryIndex);
                    System.arraycopy(modelIDs, 0, result, offset, modelIDs.length);
                }
            }
        }
        return result;
    }

    String[] extractModelId(Set<ModelSummary> models) {
        String[] result = new String[models.size()];
        int idx = -1;
        for (ModelSummary model : models) {
            result[++idx] = model.id;
        }
        return result;
    }
}
