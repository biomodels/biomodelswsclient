/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package uk.ac.ebi.biomodels.ws;

import uk.ac.ebi.biomodels.ws.dao.client.model.ModelWsClient;
import uk.ac.ebi.biomodels.ws.dao.client.search.SearchWsClient;
import uk.ac.ebi.biomodels.ws.dao.config.Config;
import uk.ac.ebi.biomodels.ws.dao.config.ProdConfig;

import java.util.Set;

/**
 * @author Mihai Glonț mglont@ebi.ac.uk
 * @author Tung Nguyen tnguyen@ebi.ac.uk
 */
public class BioModelsWSClient {
    Config config;
    ModelWsClient modelWsClient;
    SearchWsClient searchWsClient;

    public BioModelsWSClient() {
        this.config = new ProdConfig();
        this.modelWsClient = new ModelWsClient(config);
        this.searchWsClient = new SearchWsClient(config);
    }

    public BioModelsWSClient(Config config) {
        this.config = config;
        this.modelWsClient = new ModelWsClient(config);
        this.searchWsClient = new SearchWsClient(config);
    }

    public BioModelsWSClient(Config config, ModelWsClient modelWsClient, SearchWsClient searchWsClient) {
        this.config = config;
        this.modelWsClient = modelWsClient;
        this.searchWsClient = searchWsClient;
    }

    public static void main(String[] args) {
        System.out.println("Welcome BioModels Web Services Client!");
        BioModelsWSClient client = new BioModelsWSClient();
        String[] allCuratedModelIds = null;
        try {
            allCuratedModelIds = client.getAllCuratedModelsId();
        } catch (BioModelsWSException e) {
            e.printStackTrace();
        }
        for (String id: allCuratedModelIds) {
            System.out.println(id);
        }
    }

    public  String[] getAllCuratedModelsId() throws BioModelsWSException {
        try {
            return searchWsClient.getAllCuratedModelsId();
        } catch (Exception e) {
            throw new BioModelsWSException(e);
        }
    }

    public String getModelNameById(String modelId) throws BioModelsWSException {
        try {
            return modelWsClient.getModelNameByModelId(modelId);
        } catch (Exception e) {
            throw new BioModelsWSException(e);
        }
    }

    public SimpleModel getSimpleModelById(String modelId) throws BioModelsWSException {
        try {
            return modelWsClient.getModel(modelId);
        } catch (Exception e) {
            throw new BioModelsWSException(e);
        }
    }

    public String getModelSBMLById(String modelId) throws BioModelsWSException {
        try {
            return modelWsClient.getModelSBMLById(modelId);
        } catch (Exception e) {
            throw new BioModelsWSException(e);
        }
    }

    public String getPublicationId(String modelId) throws BioModelsWSException {
        try {
            return modelWsClient.getPublicationId(modelId);
        } catch (Exception e) {
            throw new BioModelsWSException(e);
        }
    }
}
