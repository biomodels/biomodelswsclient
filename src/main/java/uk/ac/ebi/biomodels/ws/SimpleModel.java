/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package uk.ac.ebi.biomodels.ws;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import uk.ac.ebi.biomodels.ws.dao.model.Format;
import uk.ac.ebi.biomodels.ws.dao.model.History;
import uk.ac.ebi.biomodels.ws.dao.model.ModelFiles;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Mihai Glonț mglont@ebi.ac.uk
 * @author Tung Nguyen tnguyen@ebi.ac.uk
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SimpleModel {
    /** Curated identifier */
    String id;
    @JsonProperty("name")
    String name;
    @JsonProperty("description")
    String description;
    @JsonProperty("format")
    Format format;
    @JsonProperty("publication")
    JsonNode publication;
    @JsonProperty("files")
    ModelFiles files;
    @JsonProperty("history")
    History history;
    @JsonProperty("firstPublished")
    Date firstPublished;
    /** perennial model identifiers */
    @JsonProperty("submissionId")
    String submissionId;
    /* to support existing clients, we use publicationId as identifier of the associated publication */
    String publicationId;
    List<String> authors;
    List<String> encoders;
    Date lastModificationDate;

    public SimpleModel() {
    }

    public SimpleModel(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public SimpleModel(String id, String name,
                       String submissionId,
                       String publicationId,
                       List<String> encoders, List<String> authors,
                       String lastModificationDate) {
        this.id = id;
        this.name = name;
        this.submissionId = submissionId;
        this.publicationId = publicationId;
        this.encoders = encoders;
        this.authors = authors;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        try {
            // removes the ':' from the time zone section (in order to be able to use SimpleDateFormat)
            lastModificationDate = lastModificationDate.substring(0, 22) + lastModificationDate.substring(23);
            this.lastModificationDate = dateFormat.parse(lastModificationDate);
        } catch (ParseException e) {
            this.lastModificationDate = null;
        }
    }

    public SimpleModel(String id, String name, String description, Format format, JsonNode publication, ModelFiles files,
                       History history, Date firstPublished, String submissionId, String publicationId,
                       List<String> authors, List<String> encoders, Date lastModificationDate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.format = format;
        this.publication = publication;
        this.files = files;
        this.history = history;
        this.firstPublished = firstPublished;
        this.submissionId = submissionId;
        this.publicationId = publicationId;
        this.authors = authors;
        this.encoders = encoders;
        this.lastModificationDate = lastModificationDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getAuthors() {
        throw new UnsupportedOperationException();
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public List<String> getEncoders() {
        throw new UnsupportedOperationException();
    }

    public void setEncoders(List<String> encoders) {
        this.encoders = encoders;
    }

    public Date getLastModificationDate() {
        throw new UnsupportedOperationException();
    }

    public void setLastModificationDate(Date lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }
    /**
     * Get the date of last modification, in a human readable form.
     * @return date of last modification
     */
    public String getLastModificationDateStr() {
        throw new UnsupportedOperationException();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Format getFormat() {
        return format;
    }

    public void setFormat(Format format) {
        this.format = format;
    }

    public JsonNode getPublication() {
        return publication;
    }

    public void setPublication(JsonNode publication) {
        this.publication = publication;
    }

    public ModelFiles getFiles() {
        return files;
    }

    public void setFiles(ModelFiles files) {
        this.files = files;
    }

    public History getHistory() {
        return history;
    }

    public void setHistory(History history) {
        this.history = history;
    }

    public Date getFirstPublished() {
        return firstPublished;
    }

    public void setFirstPublished(Date firstPublished) {
        this.firstPublished = firstPublished;
    }

    public String getSubmissionId() {
//        throw new UnsupportedOperationException("Not implemented yet!");
        return submissionId;
    }

    public void setSubmissionId(String submissionId) {
        this.submissionId = submissionId;
    }

    public String getPublicationId() {
        return publicationId;
    }

    public void setPublicationId(String publicationId) {
        this.publicationId = publicationId;
    }
}
