/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package uk.ac.ebi.biomodels.ws.dao.client.search;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Mihai Glonț mglont@ebi.ac.uk
 * @author Tung Nguyen tnguyen@ebi.ac.uk
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Result {
    @JsonProperty("hitCount")
    Integer count = null;

    @JsonProperty("models")
    Set<ModelSummary> models = null;

    @JsonProperty("queryParameters")
    RequestParameter queryParameters;

    public Result() {
        count = 0;
    }

    public Result(Integer count, Set<ModelSummary> models, RequestParameter queryParameters) {
        this.count = count;
        this.models = models;
        this.queryParameters = queryParameters;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Set<ModelSummary> getModels() {
        return models;
    }

    public void setModels(Set<ModelSummary> models) {
        this.models = models;
    }

    public void addResults(Result results) {
        Set<ModelSummary> models = new HashSet<>();

        if (results != null) {
            if (this.models != null) {
                models.addAll(this.models);
            }
            if (results.models != null) {
                models.addAll(results.models);
            }
            count = models.size();
        }
    }

    public RequestParameter getQueryParameters() {
        return queryParameters;
    }

    public void setQueryParameters(RequestParameter queryParameters) {
        this.queryParameters = queryParameters;
    }
}
