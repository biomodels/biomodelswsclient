## Introduction

This is a backstop for existing Java projects that use the old version of BioModels' Java API or the
SOAP endpoint (https://www.ebi.ac.uk/biomodels-main/webservices) and do not have the capacity to
witch to the new BioModels RESTful programming interface (https://www.ebi.ac.uk/biomodels/dev) yet.

If you are starting a new project where integration with BioModels is envisioned *we recommend using
the REST interface rather than this Java library or the SOAP web services.*

*The SOAP endpoint and associated Java API will be retired in Q2 2019*. Read more about the
[planned upgrades to BioModels](https://www.ebi.ac.uk/biomodels/content/news/Planned-upgrades-to-BioModels).

## Features

Not all features of the old library are implemented, instead we are targeting those that have been
requested in conversations at HARMONY 2018 with users of the BioModels SOAP endpoint.

The current list of supported endpoints is:

* get all the identifiers of the curated models with `getAllCuratedModelsId()`,
* get the model name of the given model identifier with `getModelNameById(String modelId)`,
* get a `SimpleModel` object of the given model by identifier with `getSimpleModelById(String modelId)`,
* get the SBML content of a given model with `getModelSBMLById(String modelId)`,
* get the publication identifier of a given model identifier with `getPublicationId(String modelId)`.

## Getting started

### Maven, Gradle and other similar build systems

    <dependencies>
        <dependency>
            <groupId>uk.ac.ebi.biomodels.ws</groupId>
            <artifactId>BioModelsWSClient</artifactId>
            <version>1.5.0</version>
        </dependency>
    </dependencies>

    <repositories>
        <repository>
            <id>ebi-repo</id>
            <name>EBI Maven Repository</name>
            <url>http://www.ebi.ac.uk/~maven/m2repo/</url>
        </repository>
    </repositories>

### Build from source

Use the Gradle wrapper to package the binaries, sources and JavaDoc

    ./gradlew assemble

The generated artefacts will be in `build/libs/`

## Support

This library will no longer be supported after the SOAP endpoint is retired, so all BioModels
clients need to upgrade at some point.

## Contact

If you encounter any issues with this project, please report them on the
[tracker](https://bitbucket.org/biomodels/biomodelswsclient/issues?status=new&status=open).

You can also contact us at biomodels-developers [at] lists [dot] sf [dot] net.
